Erlang, 3 days
====

Welcome to this course.
The syllabus can be find at
[lang/erlang](https://www.ribomation.se/lang/erlang.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to the demo programs

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/lang/erlang.git
    cd erlang

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have the latest version of Erlang OTP installed. In addition, you need a decent IDE to edit your files. 

* [Erlang Download](https://www.erlang.org/downloads)
* [MS Visual Code](https://code.visualstudio.com/download). 
  Search for and install relevant Erlang plugins for VSCode.
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download). 
  Search for and install relevant Erlang plugins for IntelliJ.
* [erlide, an Eclipse plugin](http://erlide.org/). 
  You also need to have [Eclipse](https://www.eclipse.org/downloads/eclipse-packages/) installed.


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

