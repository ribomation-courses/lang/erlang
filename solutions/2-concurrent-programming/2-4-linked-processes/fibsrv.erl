-module(fibsrv).
-export([start/0, stop/0, cache/0, clear/0, crash/0, compute/1, compute/2]).
-export([init/0, new/0, new_linked/0]).

start() -> {ok, new()}.
stop() -> send(stop).
cache() -> send(cache).
clear() -> send(clear).
crash() -> send(crash).
compute(N) -> compute(N, 15).
compute(N, T) when is_integer(N), N > 1 -> send(N, T).

send(Msg) -> send(Msg, 2).
send(Msg, MaxSeconds) ->
  case whereis(?MODULE) of
    undefined ->
      no_server;
    Pid ->
      Pid ! {req, self(), Msg},
      receive
        {reply, Result} -> Result
      after MaxSeconds * 1000 -> timeout
      end
  end.

new() ->
  spawn(?MODULE, init, []).
new_linked() ->
  spawn_link(?MODULE, init, []).

init() ->
  register(?MODULE, self()),
  io:format("[~w] started pid=~w~n", [?MODULE, self()]),
  run([]).

run(stop) ->
  done;
run(Cache) ->
  receive
    {req, Pid, Msg} ->
      {NewCache, Result} = handle(Cache, Msg),
      Pid ! {reply, Result},
      run(NewCache)
  end.

handle(_, stop) ->
  {stop, stopped};

handle(Cache, cache) ->
  {Cache, Cache};

handle(_, clear) ->
  {[], cleared};

handle(Cache, crash) ->
  {Cache, crash / Cache};

handle(Cache, I) when is_integer(I), I > 1 ->
  case lists:keyfind(I, 1, Cache) of
    {I, Result} ->
      {Cache, {I, Result}};
    false ->
      Result = numbers:fibonacci(I),
      {[{I, Result} | Cache], {I, Result}}
  end.
