-module(fibsup).
-export([start/0, stop/0]).
-export([init/0]).

-define(NAME, ?MODULE).
-define(SERVER, fibsrv).

start() -> {ok, new()}.
stop() -> ?NAME ! stop.
new() -> spawn(?MODULE, init, []).

init() ->
    case whereis(?NAME) of
        undefined -> ok;
        _         -> unregister(?NAME)
    end,
    register(?NAME, self()),
    process_flag(trap_exit, true),
    io:format("[~w] started pid=~w~n", [?MODULE, self()]),
    run(?SERVER:new_linked()).  

run(ChildPid) ->
    receive
        {'EXIT', ChildPid, Reason} ->
            io:format("[fibsup] ~w crashed. Reason=~w~n~n", [ChildPid, Reason]),
            run(?SERVER:new_linked());            
        stop ->
            ?SERVER:stop(),
            io:format("[fibsup] terminated~n"),
            done
    end.
