-module(pipeline).
-export([start/0, start/2]).
-export([producer_new/2, producer_run/3]).
-export([transformer_new/2, transformer_new/1, transformer_run/1]).
-export([consumer_new/0, consumer_run/0]).

start() -> start(3, 10).

start(NumTrans, NumMsgs) 
	when is_integer(NumTrans), NumTrans >= 0, 
	     is_integer(NumMsgs) , NumMsgs > 0 ->
	C = consumer_new(),
	T = transformer_new(NumTrans, C),
	P = producer_new(NumMsgs, T),
	{ok, P, T, C}.

producer_new(N, Next) ->
	spawn(?MODULE, producer_run, [1,N,Next]).

producer_run(I,N,Next) when I > N -> 
	Next ! done;
producer_run(I,N,Next) when I =< N ->
	Next ! I,
	producer_run(I+1, N, Next).

transformer_new(0, Next) ->
	Next;
transformer_new(N, Next) when N > 0 -> 
	T = transformer_new(Next),
	transformer_new(N-1, T).

transformer_new(Next) ->
	spawn(?MODULE, transformer_run, [Next]).

transformer_run(Next) ->
	receive
		done -> 
			Next ! done;
		N when is_integer(N) -> 
			Next ! 2 * N,
			transformer_run(Next)
	end.
	
consumer_new() ->
	spawn(?MODULE, consumer_run, []).

consumer_run() ->
	receive
		done -> 
			ok;
		N when is_integer(N) -> 
			io:format("~B~n", [N]),
			consumer_run()
	end.
