-module(prodcons).
-export([start/0, start/1, 
		consumer_new/0, consumer_run/0, 
		producer_new/2, producer_run/3]).

start() -> start(10).

start(N) when is_integer(N), N > 0 ->
	 producer_new(N, consumer_new()).

consumer_new() ->
	spawn(prodcons, consumer_run, []).

consumer_run() ->
	receive
		done -> 
			ok;
		N when is_integer(N) -> 
			io:format("fib(~B) = ~B~n", [N, acc:fibonacci(N)]),
			consumer_run()
	end.

producer_new(N, Next) ->
	spawn(prodcons, producer_run, [1,N,Next]).

producer_run(I,N,Next) when I > N -> 
	Next ! done;
producer_run(I,N,Next) when I =< N ->
	Next ! I,
	producer_run(I+1, N, Next).
