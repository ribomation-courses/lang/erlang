-module(fibsrv3).
-export([start/0, compute/1, stop/0]).
-export([cache/0, clear/0, timeout/1]).
-export([srv_init/0]).

start() ->
	{ok, srv_new()}.

compute(N) when is_integer(N), N > 0 ->
	send(N).
	
stop() -> 
	send(stop).
	
cache() -> 
	send(cache).
	
clear() -> 
	send(clear).
	
timeout(MaxSecs) when is_integer(MaxSecs), MaxSecs > 0 -> 
	send({timeout, MaxSecs*1000}).
	
send(Msg) ->
	case whereis(?MODULE) of
		undefined -> 
			'No server. Call :start()';

		Srv when is_pid(Srv) ->
			Srv ! {req, self(), Msg},
			receive
				{res, Result} -> Result
				after 10*1000 -> 'TIMEOUT'
			end
	end.

srv_new() ->
	spawn(?MODULE, srv_init, []).

srv_init() ->
	register(?MODULE, self()),
	put(timeout, 5*1000),
	srv_run([]).
	
srv_run(Cache) ->
	receive
		{req, Client, stop} -> 
			Client ! {res, stopped};

		{req, Client, {timeout,Value}} ->
			ValueOld = put(timeout, Value),
			Client ! {res, {timeout_old, ValueOld}},
			srv_run(Cache);

		{req, Client, cache} ->
			Client ! {res, Cache},
			srv_run(Cache);

		{req, Client, clear} ->
			Client ! {res, cleared},
			srv_run([]);

		{req, Client, Arg} when is_integer(Arg) -> 
			{Reply, CacheNew} = handle(Arg, Cache),
			Client ! {res, Reply},
			srv_run(CacheNew);

		Unexpected ->
			io:format("[fibsrv] Got unexpected message: ~w~n", [Unexpected])
	end.

handle(Arg, Cache) ->
	case lists:keyfind(Arg, 1, Cache) of
		false ->
			Master = self(),
			Slave  = spawn(fun()-> Master ! {from_slave, numbers:fibonacci(Arg)} end),
			Result = receive
				{from_slave, Value} -> Value
				after get(timeout)  -> exit(Slave, timeout), 'TIMEOUT'
			end,
			Reply = {Arg, Result},
			{Reply, [Reply | Cache]};

		{Arg, Result} ->
			{{Arg, Result}, Cache}
	end.
