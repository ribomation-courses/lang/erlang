-module(fibsrv).
-export([start/0, compute/1, stop/0]).
-export([srv_init/0]).

start() ->
  {ok, srv_new()}.

compute(N) when is_integer(N), N > 0 ->
  send(N).

stop() ->
  send(stop).

send(Msg) ->
  case whereis(?MODULE) of
    undefined ->
      'No server. Call :start()';

    Srv when is_pid(Srv) ->
      Srv ! {req, self(), Msg},
      receive
        {res, Result} -> Result
      after 30 * 1000 -> 'TIMEOUT'
      end
  end.

srv_new() ->
  spawn(?MODULE, srv_init, []).

srv_init() ->
  register(?MODULE, self()),
  srv_run().

srv_run() ->
  receive
    {req, Client, stop} ->
      Client ! {res, stopped};

    {req, Client, Arg} ->
      Reply = {Arg, numbers:fibonacci(Arg)},
      Client ! {res, Reply},
      srv_run()
  end.
