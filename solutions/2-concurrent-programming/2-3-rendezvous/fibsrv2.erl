-module(fibsrv2).
-export([start/0, compute/1, stop/0]).
-export([cache/0, clear/0]).
-export([srv_init/0]).

start() ->
	{ok, srv_new()}.

compute(N) when is_integer(N), N > 0 ->
	send(N).
	
stop() -> 
	send(stop).
	
cache() -> 
	send(cache).
	
clear() -> 
	send(clear).
	
send(Msg) ->
	case whereis(?MODULE) of
		undefined -> 
			'No server. Call :start()';

		Srv when is_pid(Srv) ->
			Srv ! {req, self(), Msg},
			receive
				{res, Result} -> Result
				after 10*1000 -> 'TIMEOUT'
			end
	end.

srv_new() ->
	spawn(?MODULE, srv_init, []).

srv_init() ->
	register(?MODULE, self()),
	srv_run([]).
	
srv_run(Cache) ->
	receive
		{req, Client, stop} -> 
			Client ! {res, stopped};

		{req, Client, cache} ->
			Client ! {res, Cache},
			srv_run(Cache);

		{req, Client, clear} ->
			Client ! {res, cleared},
			srv_run([]);

		{req, Client, Arg} -> 
			{Reply, CacheNew} = srv_lookup(Arg, Cache),
			Client ! {res, Reply},
			srv_run(CacheNew)
	end.

srv_lookup(Arg, Cache) ->
	case lists:keyfind(Arg, 1, Cache) of
		false ->
			Reply = {Arg, numbers:fibonacci(Arg)},
			{Reply, [Reply | Cache]};

		{Arg, Result} ->
			{{Arg, Result}, Cache}
	end.
