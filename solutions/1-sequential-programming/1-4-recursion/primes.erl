-module(primes).
-export([compute/1, test/0, last/1]).

compute(N) when is_integer(N), N > 2 ->
    filter( lst:gen(2,N) ).

filter([]) -> [];
filter([P | Ns]) ->
    [P | filter( lst:strip(Ns, P) )].

test() ->
    [2,3,5,7,11,13,17,19] = compute(20),
    97 = last(compute(100)),
    test_OK.

last([]) -> [];
last([X]) -> X;
last([_|T]) -> last(T).
