-module(wd).
-export([new/0, add/3, find/2, words/1, numbers/1, test/0]).

new() -> [].

add(Object, Word, Number) ->
    [{Word,Number} | Object].

find([], _)                       -> not_found;
find([{Word,Number} | _], Word)   -> Number;
find([{Word,Number} | _], Number) -> Word;
find([_ | Object] , Item)         -> find(Object, Item).

words(Object)   -> extract(Object, 1).
numbers(Object) -> extract(Object, 2).
extract([], _)               -> [];
extract([Tuple | Object], N) -> 
    [element(N,Tuple) | extract(Object, N)].

test() ->
    Data = demo(),
    [{three,3}, {two,2}, {one,1}] = Data,
    [{foo,17}] = add(new(), foo,17),
    4 = length(add(Data, foo,17)),
    two = find(Data, 2),
    2 = find(Data, two),
    not_found = find(Data, whatever),
    [three,two,one] = words(Data),
    [3,2,1] = numbers(Data),
    test_OK.

demo() -> 
    add(add(add(new(), one,1), two,2), three,3).
