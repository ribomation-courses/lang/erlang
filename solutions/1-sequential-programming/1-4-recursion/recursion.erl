-module(recursion).
-export([print/1]).

print(N) when is_integer(N) ->
	print(1, N).

print(I, N) when I > N -> ok;
print(I, N) when I =< N ->
	io:format("~B\t~B\t~B\t~B~n", 
		[I, numbers:sum(I), numbers:fibonacci(I), numbers:factorial(I)]),
	print(I+1, N).