-module(acc).
-export([factorial/1, fibonacci/1, mklist/1]).

% --- factorial ---
factorial(N) when is_integer(N), N > 0 ->
    factorial(1, N, 1).

factorial(I, N, Result) when I > N -> 
    Result;
factorial(I, N, Product) when I =< N -> 
    factorial(I+1, N, I * Product).

% --- fibonacci ---
fibonacci(N) when is_integer(N), N > 0 ->
    fibonacci(2,N, 0, 1).

fibonacci(I, N, _, Result) when I > N -> 
    Result;
fibonacci(I, N, F0, F1) when I =< N ->
    fibonacci(I+1, N, F1, F0+F1).

% --- mklist ---
mklist(N) -> mklist(1, N, []).

mklist(I,N,List) when I > N  -> List;
mklist(I,N,List) when I =< N ->
	mklist(I, N-1, [N | List]).
