-module(lst).
-export([print/1, sum/1, prod/1, gen/1, gen/2, gen/3, remove/2, strip/2]).

print(N) when N > 0 -> print(1, N).

print(I, N) when I > N -> ok;
print(I, N) when I =< N ->
	io:format("~B\t~B\t~B\t~B~n", [
		I, numbers:sum(I), numbers:fibonacci(I), numbers:factorial(I)
	]),
	print(I+1, N).

sum([])       -> 0;
sum([N | Ns]) when is_integer(N), N>0 -> N + sum(Ns).

prod([])       -> 1;
prod([N | Ns]) -> N * prod(Ns).

gen(N)    -> gen(1, N).
gen(I, N) -> gen(I, 1, N).
gen(Start, _Step, End) when Start > End -> [];
gen(Start, Step, End) when Start =< End -> 
	[Start | gen(Start+Step, Step, End)].


remove([], _)      -> [];
remove([X | T], X) -> remove(T, X);
remove([H | T], X) -> [H | remove(T, X)].

strip([], _)                         -> [];
strip([N | Ns], P) when N rem P == 0 -> strip(Ns, P);
strip([N | Ns], P) when N rem P /= 0 -> [N | strip(Ns, P)].





