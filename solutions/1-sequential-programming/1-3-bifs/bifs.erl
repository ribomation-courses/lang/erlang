-module(bifs).
-export([greeting/1, average/1, fib/1, car_license/0, iso8601/0]).
%% -export([rchar/0, rint/0]).

greeting(Name) ->
	io:format("Hi there, ~s~n", [Name]).

average([])                     -> 0;
average(Lst) when is_list(Lst)  -> lists:sum(Lst) / length(Lst);
average(Tpl) when is_tuple(Tpl) -> average( tuple_to_list(Tpl) );
average(N)   when is_number(N)  -> N.

fib(N) when is_integer(N), N > 1 ->
	{Elapsed, Result} = timer:tc(numbers, fibonacci, [N]),
	io:format("fib(~B) = ~B, elapsed ~f seconds~n", [N, Result, Elapsed / (1000*1000)]).

car_license() ->
	string:join(io_lib:format("~c~c~c~B~B~B", 
		[rchar(), rchar(), rchar(), rint(), rint(), rint()]), "").

rchar() -> $A + random:uniform($Z - $A).
rint()  -> random:uniform(10) - 1.

iso8601() ->
	{Y,M,D} = date(),
	{H,MM,S} = time(),
	io:format("~B-~2.10.0B-~2.10.0B ~2.10.0B:~2.10.0B:~2.10.0B~n", [Y,M,D,H,MM,S]).
