-module(cars).
-export([newCar/4, toString/1, new/0, add/2, find/2, remove/2, print/1, test/0, size/1]).
-compile({no_auto_import,[size/1]}).

-record(car, {license,vendor,model,year}).
-record(registry, {cars = []}).

newCar(L, V, M, Y) -> 
    #car{license=L, vendor=V, model=M, year=Y}.

toString(Car) ->
    io_lib:format("Car{~p: ~p ~p (~B)}", 
        [Car#car.license, Car#car.vendor, Car#car.model, Car#car.year]).

new() -> 
	#registry{}.

add(Registry, Car) -> 
	Registry#registry{cars = [Car | Registry#registry.cars]}.

size(Registry) ->
	length( Registry#registry.cars ).
	
find(Registry, License) ->
    lists:keyfind(License, #car.license, Registry#registry.cars).

remove(Registry, License) ->
    Registry#registry{cars = lists:keydelete(License, #car.license, Registry#registry.cars)}.

print(Registry) ->
    lists:foreach(fun(Car)-> 
        io:format("~s~n", [toString(Car)]) 
    end, Registry#registry.cars).

test() ->
	Cars = demo(),
    N = length(Cars#registry.cars),
	N = size(Cars),
    N = size(add(Cars, newCar(rrr,bmw,x5,2014))) - 1,
    N = size(remove(Cars, xyz123)) + 1,
    s80 = element(4, find(Cars, jkl789)),
    false = find(Cars, whatever),
    print(Cars),
    test_OK.

demo() ->
	R0 = add(new(), newCar(xyz123,volvo,amazon,1966)),
	R1 = add(R0, newCar(abc123,volvo,140,1972)),
	R2 = add(R1, newCar(def123,volvo,740,1990)),
	R3 = add(R2, newCar(ghi123,volvo,850,1997)),
	R4 = add(R3, newCar(jkl789,volvo,s80,2010)),
	R5 = add(R4, newCar(mnp012,volvo,xc60,2014)),
    R5.
