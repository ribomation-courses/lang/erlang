-module(hello).
-export([msg/0, msg/1]).

msg() -> 'Tjabba habba'.

msg(Name) -> "Hi there, " ++ Name.
