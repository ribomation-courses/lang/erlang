-module(shapes).
-export([area/1, test/0]).

area( {circle, R} )    -> math:pi() * R * R;
area( {rect, A, B} )   -> A * B;
area( {triang, H, B} ) -> H * B / 2;
area( {square, S} )    -> S * S;
area( _ ) -> usage().

usage() ->
	'usage: area({circle,R}) | area({rect,A,B}) | area({triang,H,B}) | area({square,S})'.
	
test() ->
	1.0 = area({circle, 1}) / math:pi(),
	50 = area({rect, 5, 10}),
	5.0 = area({triang, 2, 5}),
	100 = area({square, 10}),
	'usage: area({circle,R}) | area({rect,A,B}) | area({triang,H,B}) | area({square,S})' = area(17),
	test_OK.
	