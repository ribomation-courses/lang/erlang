-module(closures).
-export([each/2, test_each/0, test_each/1]).
-export([for/3, test_for/0, test_for/1]).
-export([variance/1, combine/2, curry1/2, curry2/2, test_variance/0, test_combine/0]).

each(_, []) -> done;
each(F, [H | T]) -> F(H), each(F, T).

test_each() -> test_each(10).
test_each(Max) ->
  Print = fun(N) -> io:format("  ~p~n", [N]) end,
  each(Print, lists:seq(1, Max)).



for(I, N, _) when I > N -> done;
for(I, N, F) when I =< N -> F(I), for(I + 1, N, F).

test_for() -> test_for(10).
test_for(Max) ->
  F = fun(N) ->
    io:format("~B\t~B\t~B\t~B~n",
      [N, numbers:sum(N), acc:fibonacci(N), acc:factorial(N)])
      end,
  for(1, Max, F).


%% --- variance ---
variance([]) -> 0;
variance(Numbers) when is_list(Numbers) ->
  Sum = fun(K, Sum) -> K + Sum end,
  Mean = fun(L) -> lists:foldl(Sum, 0, L) / length(L) end,
  Square = fun(N) -> N * N end,
  Diff = fun(M) -> fun(I) -> I - M end end,
  Mean(lists:map(Square, lists:map(Diff(Mean(Numbers)), Numbers))).

test_variance() ->
  variance([1, 1, 2, 2, 2, 3, 3, 3, 4, 3, 2, 2, 1, 1]).


%% --- combine ---
combine(First, Last) ->
  [{F, L} || F <- First, L <- Last].

test_combine() ->
  FirstName = [anna, berit, carin, doris],
  LastNames = [andersson, bertilsson, carlsson],
  combine(FirstName, LastNames).


%% --- curry ---
curry1(BinaryClosure, Arg1) -> fun(X) -> BinaryClosure(Arg1, X) end.
curry2(BinaryClosure, Arg2) -> fun(X) -> BinaryClosure(X, Arg2) end.
