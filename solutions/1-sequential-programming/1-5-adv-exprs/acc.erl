-module(acc).
-export([factorial/1, fibonacci/1]).

factorial(N) when is_integer(N), N > 0 ->
    factorial(1, N, 1).

factorial(I, N, Result) when I > N -> 
    Result;
factorial(I, N, Product) when I =< N -> 
    factorial(I+1, N, I * Product).

fibonacci(N) when is_integer(N), N > 0 ->
    fibonacci(2,N, 0, 1).

fibonacci(I, N, _, Result) when I > N -> 
    Result;
fibonacci(I, N, F0, F1) when I =< N ->
    fibonacci(I+1, N, F1, F0+F1).
