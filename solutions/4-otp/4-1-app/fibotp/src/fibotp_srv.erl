-module(fibotp_srv).
-behaviour(gen_server).
-export([start/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(state, {}).
-define(NAME, ?MODULE).

start() -> 
  gen_server:start_link({local,?NAME}, ?MODULE, [], []).
stop()  -> 
  gen_server:cast(?NAME, stop).

init([])                     -> io:format("[~w] started~n",[?NAME]), {ok, #state{}}.
terminate(_Why, _State)      -> io:format("[~w] terminated~n",[?NAME]), ok.
code_change(_, State, _)     -> {ok, State}.

handle_call(_, _From, State) -> {reply, none, State}.
handle_cast(stop, State)     -> {stop, normal, State}.
handle_info(_Info, State)    -> {noreply, State}.
