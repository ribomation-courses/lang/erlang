{
    application,
    fibonacci,
    [
        {description, "Simple OTP Fibonacci server"},
        {vsn, "0.17"},
        {modules, [fibotp_srv,fibotp_sup,fibotp_app]},
        {registered, [fibotp_sup,fibotp_srv]},
        {applications, [kernel,stdlib]},
        {mod, {fibotp_app,[]}}
    ]
}.
