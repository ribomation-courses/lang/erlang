-module(fibotp_app).
-behaviour(application).
-export([start/2, stop/1]).

start(_, _) -> fibotp_sup:start().
stop(_)     -> ok.
