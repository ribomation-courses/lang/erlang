-module(fibotp_srv).
-behaviour(gen_server).
-export([start/0, stop/0]).
-export([compute/1, cache/0, clear/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(state, {cache = []}).
-define(NAME, ?MODULE).

compute(N) when is_integer(N), N > 0 ->
  gen_server:call(?NAME, {compute, N}).
cache() -> gen_server:call(?NAME, cache).
clear() -> gen_server:call(?NAME, clear).

start() ->
  gen_server:start_link({local, ?NAME}, ?MODULE, [], []).
stop() ->
  gen_server:cast(?NAME, stop).

init([]) -> io:format("[~w] started~n", [?NAME]), {ok, #state{}}.
terminate(_Why, _State) -> io:format("[~w] terminated~n", [?NAME]), ok.
code_change(_, State, _) -> {ok, State}.

handle_cast(stop, State) -> {stop, normal, State}.
handle_info(_Info, State) -> {noreply, State}.

handle_call(cache, _From, State) ->
  {reply, State#state.cache, State};
handle_call(clear, _From, State) ->
  {reply, cleared, State#state{cache = []}};
handle_call({compute, N}, _From, State) ->
  Cache = State#state.cache,
  case lists:keyfind(N, 1, Cache) of
    false ->
      Result = numbers:fibonacci(N),
      Reply = {N, Result},
      io:format("[fibotp] returned computed ~w~n", [Reply]),
      {reply, Reply, State#state{cache = [Reply | Cache]}};
    {N, Result} ->
      Reply = {N, Result},
      io:format("[fibotp] returned cached ~w~n", [Reply]),
      {reply, Reply, State}
  end.
