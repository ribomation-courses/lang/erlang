@echo off

set BEAM=target\beams
set MODULE=primes
set ENTRY=run

erl -noshell -pa %BEAM% -run %MODULE% %ENTRY% %* -run erlang halt
