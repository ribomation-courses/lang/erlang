@echo on

set OUT=target\beams
set SRC=src\erl
set FILES=%SRC%/numbers.erl %SRC%/list.erl %SRC%/primes.erl

mkdir %OUT%
erlc -o %OUT% -pa %OUT%  %FILES%
