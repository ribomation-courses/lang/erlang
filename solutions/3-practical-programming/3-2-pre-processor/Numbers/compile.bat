@echo on

set BEAM=target\beams
set ERL=src\erl
set HRL=src\incl
set FILES=%ERL%/numbers.erl %ERL%/list.erl %ERL%/fibonacci.erl %ERL%/primes.erl

mkdir %BEAM%
erlc -o %BEAM% -I %HRL% -Ddebug -pa %BEAM%  %FILES%
