
-ifdef(debug).
-define(PRINT(EXPR, ARG), io:format("[~s:~B] ~s ARG=~w => ~w~n", [?FILE, ?LINE, ??EXPR, ARG, EXPR])).
-else.
-define(PRINT(EXPR, ARG), io:format("~s => ~w~n", [??EXPR, EXPR])).
-endif.

-define(SLEEP(T), receive after T -> ok end).
