-module(primes).
-export([run/1, compute/1]).
-include("macro.hrl").

run([]) -> ?SLEEP(1000);
run([Arg | Args]) ->
    N = list_to_integer(Arg),
    %% Result = compute(N),
    %% io:format("primes(~B) = ~w~n", [N, Result]),
	?PRINT(primes:compute(N), N),
    run(Args).

compute(N) when is_integer(N), N > 2 ->
    filter( list:generate(2,N) ).

filter([]) -> [];
filter([P | Ns]) ->
    [P | filter( list:strip(Ns, P) )].
