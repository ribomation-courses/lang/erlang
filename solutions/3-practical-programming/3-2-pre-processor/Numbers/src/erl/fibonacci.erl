-module(fibonacci).
-export([run/1]).
-include("macro.hrl").

run([]) -> ?SLEEP(1000);
run([Arg | Args]) ->
    N = list_to_integer(Arg),
    ?PRINT(numbers:fibonacci(N), N),
    run(Args).


