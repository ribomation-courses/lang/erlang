-module(list).
-export([sum/1, prod/1, 
    generate/1, generate/2, generate/3,
    remove/2, strip/2, test/0]).

sum([]) -> 0;
sum([N | Ns]) -> N + sum(Ns).

prod([]) -> 1;
prod([N | Ns]) -> N * prod(Ns).

generate(Start, _, End) when Start > End -> 
    [];
generate(Start, Step, End) when Start =< End ->
    [Start | generate(Start+Step, Step, End)].

generate(Start, End) -> generate(Start, 1, End).
generate(End) -> generate(1, End).

remove([], _) -> [];
remove([H|T], X) when H==X -> remove(T,X);
remove([H|T], X) when H/=X -> [H | remove(T,X)].

strip([], _) -> [];
strip([H|T], N) when H rem N == 0 -> strip(T,N);
strip([H|T], N) when H rem N /= 0 -> [H | strip(T,N)].

test() ->
    55          = sum( generate(10) ),
    120         = prod( generate(5) ),
    [1,2,3,4,5] = generate(5),
    [1,2,3,5]   = remove(generate(5), 4),
    [1,2,4,5,7,8,10] = strip(generate(10), 3),
    test_ok.
