-module(filedemo).
-export([countLines/1, lineCount/1, count/1, read/1]).

countLines(Filename) -> 
    case file:open(Filename, read) of
        {error, Why} -> file:format_error(Why);
        {ok, File}   -> 
            Count = doCount(File, readLine(File), 0),
            file:close(File),
            Count
    end.

doCount(_File, eof, NumLines) -> 
    NumLines;
doCount(_File, {error, Why}, _NumLines) -> 
    throw( file:format_error(Why) );
doCount(File, _Line, NumLines) ->
    doCount(File, readLine(File), 1 + NumLines).

readLine(File) ->
    case file:read_line(File) of
        {ok, Line}   -> Line;
        eof          -> eof;
        {error, Why} -> throw(file:format_error(Why))
    end.

%% ----------------------------------
processFile(Filename, Init, ProcessLine) ->
    case file:open(Filename, read) of
        {error, Why} -> 
            throw( file:format_error(Why) );
        {ok, File}   -> 
            Result = processLine(File, readLine(File), Init, ProcessLine),
            file:close(File),
            Result
    end.

processLine(_, eof, Result, _) ->
    Result;
processLine(_, {error, Why}, _, _) ->
    throw( file:format_error(Why) );
processLine(File, Line, Result, Compute) ->
    ResultNew = Compute(string:strip(Line, right, $\n), Result),
    processLine(File, readLine(File), ResultNew, Compute).

lineCount(Filename) -> 
    processFile(Filename, 0, 
      fun(_, Cnt) -> 
        1 + Cnt 
      end).

count(Filename) -> 
    processFile(Filename, {0,0,0}, 
      fun(Line, {Lines, Words, Chars}) -> 
        {1 + Lines, string:words(Line) + Words, length(Line) + Chars}
      end).

read(Filename) -> 
    lists:reverse(processFile(Filename, "", 
      fun(Line, Content) -> 
        [Line | Content] %% Content ++ "\n" ++ Line
      end)).
