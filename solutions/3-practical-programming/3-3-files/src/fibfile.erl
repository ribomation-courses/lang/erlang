-module(fibfile).
-export([compute/0, compute/1, compute/2]).

compute() ->
  compute("../numbers.txt").

compute(File) ->
  compute(File, "../fibonacci-" ++ filename:basename(File)).

compute(InputFile, OutputFile) ->
  Input = open(InputFile, read),
  Output = open(OutputFile, write),
  io:format("Input : ~s~n", [InputFile]),
  N = process(Input, Output, read(Input), 0),
  file:close(Input),
  file:close(Output),
  io:format("Output: ~s~n", [OutputFile]),
  io:format("Lines : ~B~n", [N]).


process(_, _, eof, NumLines) -> NumLines;
process(In, Out, Line, NumLines) ->
  N = toInt(Line),
  Fib = acc:fibonacci(N),
  file:write(Out, io_lib:format("fib(~B) = ~B~n", [N, Fib])),
  process(In, Out, read(In), 1 + NumLines).


open(Filename, Mode) ->
  case file:open(Filename, Mode) of
    {error, Why} -> throw(file:format_error(Why));
    {ok, File} -> File
  end.

read(File) ->
  case file:read_line(File) of
    {ok, Line} -> Line;
    eof -> eof;
    {error, Why} -> throw(file:format_error(Why))
  end.

toInt(Line) ->
  case string:to_integer(Line) of
    {error, Why} -> throw(Why);
    {N, _} -> N
  end.
