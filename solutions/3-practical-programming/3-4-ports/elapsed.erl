-module(elapsed).
-export([time/3]).

time(Module, Function, Args) when is_list(Args) ->
	{Time, Result} = timer:tc(Module, Function, Args),
	io:format("Elapsed ~f secs~n", [Time / 1000000]), 
	Result.
