-module(fibjava).
-export([compute/1]).

compute(N) when is_integer(N), N > 1 ->
    Java = open_port({spawn,"java -cp . Fibonacci"}, 
	                 [use_stdio, {line,100}]),
    port_command(Java, integer_to_list(N) ++ "\n"),
    Result = receive
        {Java, {data,{eol,Data}}} -> Data;
        {Java, closed}        -> ok;
        {'EXIT',Java, Why}   -> throw(Why)
    end,
    io:format("Result: ~s~n", [Result]).
