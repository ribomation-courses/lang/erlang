import java.io.*;
public class Fibonacci {
  public static void main(String[] args) throws Exception {
    BufferedReader stdin = 
            new BufferedReader( new InputStreamReader(System.in) );
    int     n     = Integer.parseInt( stdin.readLine() );
    System.out.printf("fib@java(%d) = %d%n", n, fib(n));
  }
  
  static int  fib(int n) {return (n <= 2) ? 1 : fib(n-1) + fib(n-2);}
}
