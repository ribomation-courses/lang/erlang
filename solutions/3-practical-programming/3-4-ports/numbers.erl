-module(numbers).
-export([sum/1, factorial/1, fibonacci/1, test/0]).

sum(N) when N > 1 -> N*(N+1) div 2.

factorial(1) -> 1;
factorial(N) when N > 1 -> N*factorial(N-1).

fibonacci(1) -> 1;
fibonacci(2) -> 1;
fibonacci(N) when N > 2 -> fibonacci(N-1) + fibonacci(N-2).


test() ->
	 55 = sum(10),
	120 = factorial(5),
	 55 = fibonacci(10),
	test_OK.
