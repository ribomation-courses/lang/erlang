-module(echo_sup).
-behaviour(supervisor).
-export([start/0, init/1]).

-define(NAME, ?MODULE).
-define(SRV, echo_srv).

start() ->
    supervisor:start_link({local,?NAME}, ?MODULE, []).

init([]) ->
    Server = {?SRV, {?SRV, start, []},
              permanent, 2000, worker, [?SRV]},
    Restart = {one_for_one, 1, 10},
    {ok, {Restart, [Server]}}.
