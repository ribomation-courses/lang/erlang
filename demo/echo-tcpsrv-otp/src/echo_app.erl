-module(echo_app).
-behaviour(application).
-export([start/2, stop/1]).

start(_, _) -> echo_sup:start().
stop(_)     -> ok.
