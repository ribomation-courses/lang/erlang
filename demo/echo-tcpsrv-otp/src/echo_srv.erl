-module(echo_srv).
-behaviour(gen_server).
-export([start/1, start/0, count/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_PORT, 1055).
-record(state, {port, srvsock, count = 0}).

start() -> 
    start(?DEFAULT_PORT).
start(Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Port], []).
    
count() ->
    gen_server:call(?SERVER, count).
    
stop() ->
    gen_server:cast(?SERVER, stop).


init([Port]) ->
    {ok, ServerSocket} = gen_tcp:listen(Port, [{active, true}]),
    io:format("[echo] started~n"),
    {ok, #state{port = Port, srvsock = ServerSocket}, 0}.

handle_call(count, _From, State) ->
    {reply, {ok, State#state.count}, State}.

handle_cast(stop, State) ->
    {stop, normal, State}.

handle_info(timeout, State) ->
    io:format("[echo] waiting for client port=~B...~n", [State#state.port]),
    {ok, _Sock} = gen_tcp:accept(State#state.srvsock),
    io:format("[echo] client connected~n"),
    {noreply, State};
handle_info({tcp, Socket, RawData}, State) ->
    handle_data(Socket, RawData),
    RequestCount = State#state.count,
    {noreply, State#state{count = RequestCount + 1}};
handle_info({tcp_closed, _Socket}, State) ->
    io:format("[echo] client disconnected~n"),
    {noreply, State, 0}.

handle_data(Socket, Input) ->
    Msg      = re:replace(Input, "[\n\r]+$", "", [{return,list}]),
    io:format("[echo] received: '~s'~n", [Msg]),
    MsgReply = string:to_upper(Msg),
    Output   = io_lib:format("echo: ~s~n", [MsgReply]),
    gen_tcp:send(Socket, Output).

terminate(_Reason, _State) -> 
    io:format("[echo] terminated~n"),
    ok.

code_change(_OldVsn, State, _Extra) -> 
    {ok, State}.
