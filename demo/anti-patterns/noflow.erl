%% Author: jens
%% Created: 25 jul 2009
%% Description: TODO: Add description to noflow
-module(noflow).
-export([consumer/1, transformer/3, start/0]).

start() -> 
  T = create_transformer(1, create_transformer(2, create_transformer(3, create_consumer()))), 
  producer(1, T).

producer(Cnt, Next) ->
  Next ! lists:seq(1, random:uniform(1000)),
  io:format("Producer: sent=~w~n", [Cnt]),
  producer(Cnt+1, Next).

transformer(Id, Cnt, Next) -> 
  sleep(),
  receive
    Msg ->
      io:format("Transformer-~w: received=~w, queue-len=~w~n", [Id, Cnt, msgCnt()]),
      Next ! Msg,
      transformer(Id, Cnt + 1, Next)
  end.
create_transformer(Id, Next) ->
  spawn(?MODULE, transformer, [Id, 1, Next]).

consumer(Cnt) -> 
  receive
    _Msg ->
      io:format("Consumer: received=~w, queue-len=~w~n", [Cnt, msgCnt()]),
      consumer(Cnt + 1)
  end.
create_consumer() ->
  spawn(?MODULE, consumer, [1]).

msgCnt() ->
  {message_queue_len, Cnt} = process_info(self(), message_queue_len),
  Cnt.

sleep() ->
  Time = random:uniform(10),
  receive
    after Time -> ok
  end.
