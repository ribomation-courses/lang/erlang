%% Author: jens
%% Created: 14 jul 2009
%% Description: A small demo program showing the problem with 'shared data', 
%% even when managed by an Erlang process. This is the solution version.

-module(bank2).

%%
%% Exported Functions
%%
-export([start/1, run/2, account_body/1, updater_body/4]).

%%
%% start([NumUpdaters, NumOperations])
%% Used for invocation outside erl
%%
start([NumUpdaters, NumOperations]) ->
	Result = run(list_to_integer(NumUpdaters), list_to_integer(NumOperations)),
	io:format("result: ~w~n", [Result]),
	halt().


%%
%% run(NumUpdaters, NumOperations)
%%
run(NumUpdaters, NumOperations) -> 
	Account  = startAccount(),
	Bank     = self(),
	Updaters = startUpdaters(NumUpdaters, Account, NumOperations, Bank),
	Balance  = waitForUpdaters(Account, Updaters),
	closeAccount(Account),
	{finalBalance, Balance}.


%%
%% Bank
%%
waitForUpdaters(Account, Updaters) ->
	waitForUpdaters(Account, Updaters, 0).

waitForUpdaters(Account, [], NumTerminated) ->
	io:format("Received termination of ~w updaters~n", [NumTerminated]),
	getBalance(Account);
waitForUpdaters(Account, Updaters, NumTerminated) ->
	receive
		{updaterDone, Id} ->
			waitForUpdaters(Account, lists:keydelete(Id, 2, Updaters), NumTerminated + 1)
	end.

%% dropUpdater(Id, Updaters) ->
%% 	lists:keydelete(Id, 2, Updaters).
%% 	Remove = fun(Id) -> 
%% 				fun ({updater, X, _}) when Id == X -> false;
%% 					({updater, _, _})              -> true
%% 				end
%% 			 end,
%% 	lists:filter(Remove(Id), Updaters).


%%
%% Account
%%
account_body(Balance) ->
	receive
		{getBalance, Sender, _} -> 
			Sender ! {reply, Balance},
			account_body(Balance)
		;	
		{updateBalance, Sender, Value} ->
			NewBalance = Balance + Value,
			Sender ! {reply, NewBalance},
			account_body(NewBalance)
		;
		{shutdown, Sender, _} ->
			Sender ! {reply, terminated},
			io:format("Account terminated, balance=~w~n", [Balance]),
			done
	end.

startAccount() ->
	io:format("Starting account ~n", []),
	{account, spawn(bank2, account_body, [0])}.

getBalance(Account)           -> send(Account, getBalance, void).
updateBalance(Account, Value) -> send(Account, updateBalance, Value).
closeAccount(Account)         -> send(Account, shutdown, void).


%%
%% Updater
%%
update(_Id, _Acc, 0, _Op) -> 
	done;
update(Id, Account, NumOperations, Amount) when NumOperations > 0 ->
	updateBalance(Account, Amount),	%READ/(MODIFY)/WRITE
	update(Id, Account, NumOperations-1, Amount).

updater_body(Id, Account, NumOperations, Bank) ->
	%io:format("Updater ~w started (~w)~n", [Id, self()]),
	update(Id, Account, NumOperations, +100),
	update(Id, Account, NumOperations, -100),
	Bank ! {updaterDone, Id}.
	%io:format("Updater ~w terminated (~w)~n", [Id, self()]).

startUpdater(Id, Account, NumOperations, Bank) ->
	{updater, Id, spawn(bank2, updater_body, [Id, Account, NumOperations, Bank])}.

startUpdaters(NumUpdaters, Account, NumOperations, Bank) ->
	io:format("Launching ~w updaters that will perform ~w updates each~n", [NumUpdaters, NumOperations]),
	[startUpdater(Id, Account, NumOperations, Bank) || Id <- lists:seq(1, NumUpdaters)].


%%
%% Util
%%
send({_Type, Pid}, Operation, Argument) ->
	Pid ! {Operation, self(), Argument},
	receive
		{reply, Value} -> Value
	end.

