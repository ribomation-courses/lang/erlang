%% Author: jens
%% Created: 24 jul 2009
%% Description: TODO: Add description to nomsg
-module(nomsg).
-export([start/0, consumer/1]).

start() ->
  C = spawn(?MODULE, consumer, [1]),
  producer(1, C).

producer(N, C) -> 
  C ! foo,
  C ! lists:seq(1, random:uniform(10000)),
  C ! {random:uniform(10000),random:uniform(10000),random:uniform(10000),random:uniform(10000)},
  io:format("~w) Producer sent a bunch of messages~n", [N]),
  producer(N+1, C).

consumer(N) -> 
  {message_queue_len, MessageCount} = process_info(self(), message_queue_len),
  io:format("~w) Consumer: ~w messages in queue~n", [N, MessageCount]),
  receive
    Msg when is_atom(Msg) ->
      consumer(N+1)
  end.

