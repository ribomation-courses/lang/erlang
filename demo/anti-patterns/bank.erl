%% Author: jens
%% Created: 14 jul 2009
%% Description: A small demo program showing the problem with 'shared data', 
%% even when managed by an Erlang process.
-module(bank).

%%
%% Exported Functions
%%
-export([start/1, run/2, account_body/1, updater_body/4]).

%%
%% start([NumUpdaters, NumOperations])
%% Used for invocation outside erl
%%
start([NumUpdaters, NumOperations]) ->
	Result = run(list_to_integer(NumUpdaters), list_to_integer(NumOperations)),
	io:format("result: ~w~n", [Result]),
	halt().


%%
%% run(NumUpdaters, NumOperations)
%%
run(NumUpdaters, NumOperations) -> 
	Account  = startAccount(),
	Bank     = self(),
	Updaters = startUpdaters(NumUpdaters, Account, NumOperations, Bank),
	Balance  = waitForUpdaters(Account, Updaters),
	closeAccount(Account),
	{finalBalance, Balance}.


%%
%% Bank
%%
waitForUpdaters(Account, []) ->
	getBalance(Account);
waitForUpdaters(Account, Updaters) ->
	receive
		{updaterDone, Id} ->
			waitForUpdaters(Account, dropUpdater(Id, Updaters))
	end.

dropUpdater(Id, Updaters) ->
	Remove = fun(I) -> 
				fun ({updater, X, _}) when I == X -> false;
					({updater, _, _})             -> true
				end
			 end,
	lists:filter(Remove(Id), Updaters).


%%
%% Account
%%
account_body(Balance) ->
	receive
		{getBalance, Sender, _} -> 
			Sender ! {reply, Balance},
			account_body(Balance)
		;	
		{setBalance, Sender, Value} ->
			Sender ! {reply, Value},
			account_body(Value)
		;
		{shutdown, Sender, _} ->
			Sender ! {reply, terminated},
			io:format("Account terminated, balance=~w~n", [Balance]),
			done
	end.

startAccount() ->
	io:format("Starting account ~n", []),
	{account, spawn(bank, account_body, [0])}.

getBalance(Account)        -> send(Account, getBalance, void).
setBalance(Account, Value) -> send(Account, setBalance, Value).
closeAccount(Account)      -> send(Account, shutdown, void).


%%
%% Updater
%%
update(_Id, _Acc, 0, _Op) -> 
	done;
update(Id, Account, NumOperations, Operation) when NumOperations > 0 ->
	Balance    = getBalance(Account),	%READ
	NewBalance = Operation(Balance),	%MODIFY
	setBalance(Account, NewBalance),	%WRITE
	update(Id, Account, NumOperations-1, Operation).

updater_body(Id, Account, NumOperations, Bank) ->
	%io:format("Updater ~w started (~w)~n", [Id, self()]),
	update(Id, Account, NumOperations, fun(B) -> B + 100 end),
	update(Id, Account, NumOperations, fun(B) -> B - 100 end),
	Bank ! {updaterDone, Id}.
	%io:format("Updater ~w terminated (~w)~n", [Id, self()]).

startUpdater(Id, Account, NumOperations, Bank) ->
	{updater, Id, spawn(bank, updater_body, [Id, Account, NumOperations, Bank])}.

startUpdaters(NumUpdaters, Account, NumOperations, Bank) ->
	[startUpdater(Id, Account, NumOperations, Bank) || Id <- lists:seq(1, NumUpdaters)].


%%
%% Util
%%
send({_Type, Pid}, Operation, Argument) ->
	Pid ! {Operation, self(), Argument},
	receive
		{reply, Value} -> Value
	end.

