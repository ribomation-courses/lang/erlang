-module(pingpong).
-export([start/0, start/1]).
-export([driver_run/1, fsm_run/1]).

start()      -> start(false).
start(debug) -> start(true);
start(Debug) ->
    FSM = fsm_new(Debug),
    Driver = driver_new(FSM),
    {ok, FSM, Driver}.

driver_new(Pid) ->
    spawn(?MODULE, driver_run, [Pid]).
driver_run(Pid) ->
    Pid ! {event,self()}, y(),
    driver_run(Pid).

fsm_new(Debug) ->
    spawn(?MODULE, fsm_run, [Debug]).
fsm_run(Debug) ->
    d(Debug),
    log(fsm),
    receive
        {event,Pid} -> Pid ! ack, state1("caramba")
    end,
    state2("").

state1(Msg) ->
    log(state_1),
    receive
        {event,Pid} -> Pid ! ack, state2(Msg)
    end,
    state1("").

state2(Info) ->
    log(state_2),
    receive
        {event,Pid} -> Pid ! ack, state3(Info)
        after 0 -> state1(Info)
    end,
    state2("").

state3(Data) ->
    log(state_3),
    receive
        {event,Pid} -> Pid ! ack, state1(Data)
        after 0 -> state1(Data)
    end,
    state3("").

y()    -> receive _ -> ok after 1 -> ok end.
i(Key) -> {Key, Value} = process_info(self(), Key), Value.
d(D)   -> put(debug,D).
log(S) ->
    case get(debug) of
        true  -> 
            io:format("~w: stk=~B, mem=~B, tot=~B, msg=~B~n", 
                [S, i(stack_size), i(heap_size), i(total_heap_size), 
                 i(message_queue_len)]);
        _ -> ok
    end.
