%% Author: jens
%% Created: 24 jul 2009
%% Description: TODO: Add description to zombie
-module(zombie).
-export([consumer/0, transformer/1, producer/1]).


consumer() ->
  receive
    N when is_integer(N) ->
      io:format("Consumer: ~w~n", [N]),
      consumer();
    stop -> ok
  end.

transformer(Next) ->
  receive
    N when is_integer(N) ->
      Next ! 2 * N,
      transformer(Next);
    stop -> 
      Next ! stop,
      ok
  end.


producer(N) when is_integer(N), N > 1 ->
  io:format("BEFORE: process count=~w~n", [length( processes() )]),
  C = spawn(?MODULE, consumer, []),
  T = createTransformers(N, C),
  io:format("AFTER: process count=~w~n", [length( processes() )]),
  T ! N,
  T ! stop,
  ok.

createTransformers(1, T) ->
  spawn(?MODULE, transformer, [T]);
createTransformers(N, T) ->
  spawn(?MODULE, transformer, [T]),
  createTransformers(N-1, spawn(?MODULE, transformer, [T])).
